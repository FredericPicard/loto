
public class Loto {

    int[] grille;
    boolean bingo;
    int score, compteur;

    public Loto() {
        grille = new int[]{1, 7, 16, 24, 39, 49};     //combinaison testé avec les extremités 1 et 49 pour etre sur que toute la plage de numero est bonne
        bingo = false;
        score = 0;
        compteur = 0;
    }

    public int tourDeJeu() {

        do {
            compteur = compteur + 1;
            int[] tirageDeLaSemaine = new Tirage().resultat();

            bingo = new Compare().gagne(tirageDeLaSemaine, grille);

            if (bingo) {

                //******************************** affichage des resultat *******************************
                System.out.println("**********         gagné !!!!       ************");
                System.out.println("tirage :");

                for (int i = 0; i < tirageDeLaSemaine.length; i++) {
                    System.out.print(" / " + tirageDeLaSemaine[i]);  //juste pour voir le resultat
                }
                System.out.println();
                System.out.println("grille :");
                for (int i = 0; i < tirageDeLaSemaine.length; i++) {
                    System.out.print(" / " + tirageDeLaSemaine[i]);  //juste pour voir la grille demandé
                }
                System.out.println();
                System.out.println("compteur =" + compteur);
                // ******************************* fin affichage des resultat *****************************
            }
        }

        while (!bingo);

        return compteur;
    }
}
