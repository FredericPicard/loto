
public class Compare {

    public boolean gagne(int[] tir, int[] gril) {

        for (int i = 0; i < tir.length; i++) {
            if (tir[i] != gril[i]) {
                return false;
            }
        }
        return true;
    }
}
